import React from 'react';
import { Text, View } from 'react-native';
import Header from'../components/Header.js';
import SuggestionList from '../../videos/containers/SuggestionList.js'
import API from '../../../utils/Api.js';

class Home extends React.Component {

    componentDidMount() {
        API.getSuggestion(4)
    }

    render(){
        return (    
            <View>
                <Header>
                    <Text>The1984</Text>
                </Header>
                <SuggestionList/>
            </View>
        )
    }

}

export default Home;