import React from 'react';
import { FlatList, Text, View } from 'react-native';
import Layout from '../components/SuggestionListLayout.js';
import Empty from '../components/Empty.js';
import Separator from '../components/VerticalSeparator.js';
import Suggestion from '../components/Suggestion.js';

class SuggestionList extends React.Component{

  renderEmpty = () => <Empty text="No hay sugerencias"/>

  itemSeparetor = () => <Separator color="green"/>

  renderItem = ({ item }) => {
    return(
      <Suggestion {...item} />
    )
  }

  render(){

    const list = [
      {
      'title': 'Volver al futuro',
      'key' : '1'
      },
      {
      'title' : 'Titanic',
      'key' : '2'
      }
    ]

    return(
      <View>
        <Layout
            title = "Recomendado para ti"
        >
            <FlatList
              data = { list }
              ListEmptyComponent = { this.renderEmpty }
              ItemSeparatorComponent = { this.itemSeparetor }
              renderItem = { this.renderItem }   
            />
        </Layout>
      </View>
    )
  }

}

export default SuggestionList;