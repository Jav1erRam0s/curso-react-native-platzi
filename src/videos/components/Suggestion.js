import React from 'react';
import { Text, Image, View, StyleSheet } from 'react-native';

function Suggestion (props) {
  return (
    <View style={ styles.container }>
      <View style={ styles.left }>
        <Image
          style={styles.cover}
          source={require('../../../assets/The1984.png')}
        />       
        <View style={ styles.genre }>
          <Text style={ styles.genreText }>Accion</Text>
        </View>
      </View>
      <View style={ styles.right }>
        <Text style={ styles.title }>The truman show</Text>
        <Text style={ styles.year }>1999</Text>
        <Text style={ styles.rating }>5 estrellas</Text>  
      </View> 
    </View>
  ) 
}

const styles = StyleSheet.create({
    container : {
        flexDirection: 'row'
    },
    left : {

    },
    cover : {
        height: 150,
        width: 100,
        resizeMode: 'contain'
    },
    right : {
        padding: 10,
        justifyContent: 'space-between'
    },
    title : {
        fontSize:18,
        color: '#44546b'
    },
    year : {
        backgroundColor: '#70b124',
        paddingVertical: 4,
        paddingHorizontal: 6,
        color: 'white',
        fontSize: 11,
        borderRadius: 5,
        overflow: 'hidden',
        alignSelf: 'flex-start'
    },
    rating : {
        color: '#6b6b6b',
        fontSize: 14,
        fontWeight: 'bold'
    },
    genre: {
        position: 'absolute',
        left: 0,
        top: 0,
        backgroundColor: 'black',
        paddingVertical: 5,
        paddingHorizontal: 7
    },
    genreText: {
        color: 'white',
        fontSize: 11    
    }
})

export default Suggestion;