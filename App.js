import * as React from "react";
import { View, Text, Image, StyleSheet, Button, TouchableOpacity, Platform, Alert } from "react-native";
import The1984 from './assets/The1984.png'
import Home from './src/sections/containers/Home.js'

const App = () => {

  return (

    <View style={ styles.container }>
    
      <Home/>

    </View>

    /*
    <View style={ styles.container }>
    
      <Home/>

      <Text style={ styles.title }>Hello World!</Text>
      
      <Image
        style = { styles.image }
        source={{ uri : 'https://picsum.photos/200/200' }}
        //source={ The1984 }
      />

      //BUTTON DE REACT NATIVE
      <Button
        onPress={() => Alert.alert('Simple Button pressed')}
        title="Press me"
        color="#841584"
        accessibilityLabel="Learn more about this purple button"
      />
    
      
      <TouchableOpacity
        style={ styles.button }
        onPress={() => console.log("Se presiono el button TouchableOpacity")}
      >
        <Text style={ styles.buttonText }>Presioname</Text>
      </TouchableOpacity>
 
    </View>
    */
    
  );

};
/*
const styles = StyleSheet.create({
  container : { flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: "#292929" },
  title : { fontSize: 30 , color:"#FFFFFF"},
  image : { height: 200, width: 200, borderRadius: 100 },
  button : { 
    //backgroundColor: Platform.select({
      //ios: 'green',
      //android: 'blue'
    //}),
    backgroundColor: 'green',
    padding: 7,
    marginTop: 10,
    borderRadius: 5
  },
  buttonText : {
    color: "#FFFFFF",
    fontSize: 20
  }
})
*/

const styles = StyleSheet.create({
  container : { flex: 1, backgroundColor: "gray" },
})

export default App;