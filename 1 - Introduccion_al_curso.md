
# ************************************************************************************************** #
# ***********************************  El poder de React Native  *********************************** #
# ************************************************************************************************** #

React Native se parece mucho a reactjs (en codigo) excepto por:

Dentro de la libreria 'react-native' tenemos componentes que nos van a ayudar a construir la interfaz
en nuestras aplicaciones moviles. Un Text (componente) es como un <p>...</p> en html. Otro ej. es el 
componente View que es su equivalente a un <div>...</div>.

Entonces que diferencia hay? Componentes nuevos de 'react-native'. Que son exclusivos para el 
desarrollo de interfaces movile.

Algunos componentes:

. View / ScrollView / SafeAreaView
    -ScrollView : Permite scrollear un 'div' 
    -SafeAreaView : Podemos darle soporte a telefonos que tienen un notch. Ej. Iphone 10 o X.
. Text / TextInput
    -TextImput : Input de formulario.
. Image / ImageBackground
    -Image: Permite traer imagenes. Desde una ruta local, relativa o absoluta.  
. FlatList / SectionLst
    Nos sirve para ordenar nuestros elementos.
. Touchable Highligth / Opacity / WithOutFeedBack
    Componentes que nos van a ayudar a manejar las presiones (touch) dentro de nuestro telefono.
. Animated
    Vamos a encontrar formas y componentes para animar interfaces.
. fetch , async await , sockets
    fetch : Es un modulo de JavaScript con el cual podemos hacer peticiones, algo como AJAX. De hecho
    muchisimo mejor.
    Funciones asincronas para hacer peticiones http.
. Platform
    Sirve para que podamos identificar si estamos en iOS o en Android y asi poder cambiar algun tipo 
    de estilo. Ej. quiero el boton verde para dispositivos Android y Negros para iOS.
. AsyncStorage
    Es la tecnologia como tenemos en la web al LocalStorage, que nos ayuda a respaldar datos dentro 
    de nuestro dispositivo (algo asi como bd local) y por ej. cuando nuestra aplicacion se cierre y 
    la volvamos a abrir, podamos consultar esos datos que ya fueron previamente guardados.

* ¿Cuando usar React Native? 

Rta.: La velocidad de desarrollo es muy importante (ej. Instagram). ¿Por que aprender una tecnologia 
para una aplicacion web y otra para nuestra aplicacion movile? ¿Por que no aprendemos una sola y al 
mismo tiempo tenemos soporte en equipo para ambas app?

* ¿Con React Native ya no hace falta aprender java y objective-c (codigo nativo)?

Rta.: Dile NO a la pereza. Al final del dia el codigo nativo es el que se esta moviendo dentro de 
nuestro telefono. Mientra mas lo entendamos, vamos a hacer personas mas atractivas dentro del mundo 
profesional. Por ende podemos resolver problemas mas sofisticado.

* ¿Como funciona?

Como es que con JavaScript podemos hacer aplicaciones totalmente nativas? Esto es gracias al RCT 
Bridge. Es una abreviacion React RCT y Bridge = puente. Entonces nuestro codigo JavaScript va a pasar 
por un puente que fue programado por el equipo Core de React-Native para que nos podamos conectar a 
los modulos antiguos.

React Native y Developer experience

-Por ej. Hot/live reloading. Recarga en la web, los cambios realizados en tiempo real.
-Depuracion de JavaScript.
-Inspector de elementos.
-Network Inspector: Depuracion de la peticiones a la red. Por ej. las peticiones http request, los 
                    fetch, los sockets.
-Stack Trace.

* Quien usa React Native?

UBER eats, Instagram, Facebook, Wix, Skype, Pinterest, Platzi, ...

# ************************************************************************************************** #
# ***********************************  Bienvenido a Platzi Video  ********************************** #
# ************************************************************************************************** #

Vamos a desarrollar un sistema al estilo Netflix, pero de Platzi, con datos reales haciendo uso de 
peticiones htttp con redux.

# *************************************************************************************************** #